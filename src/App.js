import React, { useEffect, useReducer } from "react";

import { Container } from "reactstrap";

// react-router-dom3
import { Route, BrowserRouter as Router, Routes } from "react-router-dom";

// react toastify stuffs
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

// bootstrap css
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

// firebase stuffs
//TODO: DONE import firebase config and firebase database
import { initializeApp } from "firebase/app";
import "firebase/database";
import { getDatabase, onValue, ref } from "firebase/database";
import "firebase/storage";
import { firebaseConfig } from "./utils/config";

// components
import Footer from "./layout/Footer";
import Header from "./layout/Header";
import AddContact from "./pages/AddContact";
import Contacts from "./pages/Contacts";
import PageNotFound from "./pages/PageNotFound";
import ViewContact from "./pages/ViewContact";

// context api stuffs
import { ContactContext } from "./context/Context";
import { SET_CONTACT, SET_LOADING } from "./context/action.types";
import { ContactReducer } from "./context/reducer";

const db = getDatabase();

//initlizeing firebase app with the firebase config which are in ./utils/firebaseConfig
//TODO: DONE initialize FIREBASE
initializeApp(firebaseConfig);
// first state to provide in react reducer
const initialState = {
  contacts: [],
  contact: {},
  contactToUpdate: null,
  contactToUpdateKey: null,
  isLoading: false,
};

const App = () => {
  const [state, dispatch] = useReducer(ContactReducer, initialState);

  // will get contacts from firebase and set it on state contacts array
  const getContacts = async () => {
    dispatch({
      type: SET_LOADING,
      payload: true,
    });

    const contactsRef = ref(db, "/contacts");

    onValue(contactsRef, (snapshot) => {
      // const data = snapshot.val();
      // updateStarCount(postElement, data);
      dispatch({
        type: SET_CONTACT,
        payload: snapshot.val(),
      });
      dispatch({
        type: SET_LOADING,
        payload: false,
      });
    });

    // const contactsRef = firebase.database().ref("/contacts");
    // contactsRef.on("value", (snapshot) => {
    //   dispatch({
    //     type: SET_CONTACT,
    //     payload: snapshot.val(),
    //   });
    //   dispatch({
    //     type: SET_LOADING,
    //     payload: false,
    //   });
    // });
  };
  // getting contact  when component did mount
  useEffect(() => {
    getContacts();
  }, []);

  return (
    <Router>
      <ContactContext.Provider value={{ state, dispatch }}>
        <ToastContainer />
        <Header />
        <Container>
          <Routes>
            <Route exact path="/contact/add" component={AddContact} />
            <Route exact path="/contact/view" component={ViewContact} />
            <Route exact path="/" component={Contacts} />
            <Route exact path="*" component={PageNotFound} />
          </Routes>
        </Container>

        <Footer />
      </ContactContext.Provider>
    </Router>
  );
};

export default App;
