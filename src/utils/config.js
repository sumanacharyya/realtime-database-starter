// Import the functions you need from the SDKs you need
// import { initializeApp } from "firebase/app";
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
export const firebaseConfig = {
  apiKey: "AIzaSyCWECOwZH1-1PjvAk9PMGnfv4APsm-abNQ",
  authDomain: "realtime-contacts-875ab.firebaseapp.com",
  databaseURL: "https://realtime-contacts-875ab-default-rtdb.firebaseio.com",
  projectId: "realtime-contacts-875ab",
  storageBucket: "realtime-contacts-875ab.appspot.com",
  messagingSenderId: "66567948629",
  appId: "1:66567948629:web:8513fd5cdea24ada0387dc",
};

// Initialize Firebase
// const app = initializeApp(firebaseConfig);

//image configuration
export const imageConfig = {
  quality: 0.2,
  maxWidth: 800,
  maxHeight: 600,
  autoRotate: true,
};
